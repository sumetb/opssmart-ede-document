# OpsSmart EDE Document

The documentation is made to describe OpsSmart EDE.

## Method POST

### Request

**Authorization** Bearer Token

| Key | Value |
| ------ | ------ |
| Token | xxx |

`Token` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; encrypted Bearer Token provided by OpsSmart (OpsSmart API Token)

**Request Headers**

| Key | Value |
| ------ | ------ |
| UserName | xxx |
| Password | xxx |
| APIFormat | JSON or XML |

`UserName` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart API User Name who registered in OpsSmart

`Password` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart API Password

`APIFormat` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; schema type of request body can be only `JSON` for now

### Body

```json
{
  "Document": {
    "RequestDateTime": "2021/10/26 16:44:38:118",
    "Sender": "DEVADMIN",
    "CapsuleFileName": "AP-Betag-10_DC573.txt",
    "SessionID": "teebarzznoy4vsag3rp0enxd",
    "ActionType": "REALTIME",
    "DataObject": [
      {
        "ObjectID": "22",
        "ObjectName": "SH Manufacturer UDF",
        "ObjectType": "SINGLE",
        "TemplateName": "SH_Manufacturer_S",
        "AttributeProperties": {
          "Attribute": [
            {
              "Name": "Status",
              "Type": "STRING",
              "MaxLength": "50"
            },
            {
              "Name": "EntityType Code",
              "Type": "STRING",
              "MaxLength": "50.0"
            },
            {
              "Name": "EntityType Name",
              "Type": "STRING",
              "MaxLength": "249.0"
            },
            {
              "Name": "Entity Code",
              "Type": "STRING",
              "MaxLength": "50.0"
            },
            {
              "Name": "Entity Name",
              "Type": "STRING",
              "MaxLength": "249.0"
            },
            {
              "Name": "E-Mail",
              "Type": "STRING",
              "MaxLength": "100.0"
            },
            {
              "Name": "Effective Date",
              "Type": "DATE",
              "Format": "yyyy-MM-dd",
              "MaxLength": "50.0"
            },
            {
              "Name": "Plant Name-Eng",
              "Type": "STRING",
              "MaxLength": "150.0"
            },
            {
              "Name": "Plant Picture",
              "Type": "ATTACHMENT",
              "MaxLength": "100.0"
            }
          ]
        },
        "TotalRecord": "1",
        "DataSets": {
          "DataRecord": [
            {
              "RowID": 1,
              "Attributes": {
                "Attribute": [
                  {
                    "Name": "Status",
                    "Value": "OPEN"
                  },
                  {
                    "Name": "EntityType Code",
                    "Value": "ORGN"
                  },
                  {
                    "Name": "EntityType Name",
                    "Value": "10"
                  },
                  {
                    "Name": "Entity Code",
                    "Value": "AP-Betag-10"
                  },
                  {
                    "Name": "Entity Name",
                    "Value": "Omnoi"
                  },
                  {
                    "Name": "E-Mail",
                    "Value": "yonusarj@betagro.com"
                  },
                  {
                    "Name": "Effective Date",
                    "Value": "2014-01-01"
                  },
                  {
                    "Name": "Plant Name-Eng",
                    "Value": "Better Foods Co.,Ltd."
                  },
                  {
                    "Name": "Plant Picture",
                    "Value": "AP-Betag-10_20150925173926247.jpg"
                  }
                ]
              }
            }
          ]
        }
      },
      {
        "ObjectID": "422",
        "ObjectName": "SH Manufacturer Certificate",
        "ObjectType": "MULTI",
        "TemplateName": "SH_Manufacturer_M",
        "AttributeProperties": {
          "Attribute": [
            {
              "Name": "Status",
              "Type": "STRING",
              "MaxLength": "50"
            },
            {
              "Name": "Certificate ID",
              "Type": "STRING",
              "MaxLength": "50.0"
            },
            {
              "Name": "Certificate Name",
              "Type": "STRING",
              "MaxLength": "150.0"
            },
            {
              "Name": "EntityType Code",
              "Type": "STRING",
              "MaxLength": "50.0"
            },
            {
              "Name": "EntityType Name",
              "Type": "STRING",
              "MaxLength": "249.0"
            },
            {
              "Name": "Entity Code",
              "Type": "STRING",
              "MaxLength": "50.0"
            },
            {
              "Name": "Entity Name",
              "Type": "STRING",
              "MaxLength": "249.0"
            }
          ]
        },
        "TotalRecord": "2",
        "DataSets": {
          "DataRecord": [
            {
              "RowID": 1,
              "Attributes": {
                "Attribute": [
                  {
                    "Name": "Status",
                    "Value": "OPEN"
                  },
                  {
                    "Name": "Certificate ID",
                    "Value": "01"
                  },
                  {
                    "Name": "Certificate Name",
                    "Value": "GMP"
                  },
                  {
                    "Name": "EntityType Code",
                    "Value": "ORGN"
                  },
                  {
                    "Name": "EntityType Name",
                    "Value": "10"
                  },
                  {
                    "Name": "Entity Code",
                    "Value": "AP-Betag-10"
                  },
                  {
                    "Name": "Entity Name",
                    "Value": "Omnoi"
                  }
                ]
              }
            },
            {
              "RowID": 2,
              "Attributes": {
                "Attribute": [
                  {
                    "Name": "Status",
                    "Value": "OPEN"
                  },
                  {
                    "Name": "Certificate ID",
                    "Value": "02"
                  },
                  {
                    "Name": "Certificate Name",
                    "Value": "HACCP"
                  },
                  {
                    "Name": "EntityType Code",
                    "Value": "ORGN"
                  },
                  {
                    "Name": "EntityType Name",
                    "Value": "10"
                  },
                  {
                    "Name": "Entity Code",
                    "Value": "AP-Betag-10"
                  },
                  {
                    "Name": "Entity Name",
                    "Value": "Omnoi"
                  }
                ]
              }
            } 
          ]
        }
      }
    ],
    "Contents": {
      "Attribute": [
        {
          "Name": "AP-Betag-10_20150925173926247.jpg",
          "Type": "jpg",
          "Value": "(string base 64)",
          "MimeType": "image/pjpeg"
        }
      ]
    }
  }
}
```

`Document` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of all Objects

`RequestDateTime` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Datetime while sending the request, format `yyyy/MM/dd HH:mm:ss:fff`

`Sender` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; User Name of Sender

`CapsuleFileName` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Import Data Capsule FILE_NAME

`SessionID` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Browser or Program session

`ActionType` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Fixed Value (REALTIME)

`DataObject` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain detial of DDOT and Template

`ObjectID` (optional)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Data Object ID

`ObjectName` (optional)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Data Object Name

`ObjectType` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Signle or Multi Value DataSets

`TemplateName` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Data Capsule Template Name

`AttributeProperties` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain details of every attributes in the template

`Attribute` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain details of the attribute

`Name` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Attribute ATTR_NAME

`Type` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Attribute DATA_TYPE

`MaxLength` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; OpsSmart Attribute MAX_LENGTH

`TotalRecord` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Total Record to import

`DataSets` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of DataRecord

`DataRecord`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain detial of every records in the template

`RowID`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Record Row Number

`Attribute`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Name = "OpsSmart Attribute ATTR_NAME", Value = Actual Data

`Contents` (optional)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Parent of Data that contain details of every attachment attributes

`Name` (required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Attachment Value (e.g. "Plant Picture Value = AP-Betag-10_20150925173926247.jpg")

`Type`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; Attachment Data Format (e.g. ".jpg", ".docx", ".xlsx")

`Value`(required)<br />
&nbsp;&nbsp;&nbsp;&nbsp; String base 64 (e.g. "data:image/pjpeg;base64,/9j/4AAQSkZJRgABAQEAMwA0AAD/4QAWRXhpZgAASUkqAAgAAAAAAAAAAA..")

### Response
```json
{
  "Document": {
    "ResponseDateTime": "2021-10-26 10:22:15:509",
    "Sender": "OpsSmartSender",
    "CapsuleFileName": "filename.txt",
    "SessionID": "098f6bcd4621d373cade4e832627b4f6",
    "ActionType": "REALTIME",
    "DataObject": [
      {
        "ObjectName": "Test_DDOT",
        "ObjectType": "SINGLE",
        "TemplateName": "TP_PS LabSwab34_S",
        "AttributeProperties": {
          "Attribute": [
            {
              "Name": "xxx",
              "Type": "STRING",
              "MaxLength": "30"
            },
            {
              "Name": "xxxx",
              "Type": "DATE",
              "MaxLength": "50.0",
              "Format": "yyyy-MM-dd"
            }
          ]
        },
        "DataSets": {
          "DataRecord": [
            {
              "RowID": 1,
              "Attributes": {
                "Attribute": [
                  {
                    "Name": "xxx",
                    "Value": "OPEN"
                  },
                  {
                    "Name": "xxxx",
                    "Value": "2018-05-02"
                  }
                ]
              }
            }
          ]
        }
      }
    ],
    "Contents": {
      "Attribute": []
    },
    "ResponseCode": "0000",
    "ResponseDesc": "Process successfully completed."
  }
}
```
**Response Code**

| RESPONSE_CODE | DESCRIPTION |
| ------ | ------ |
| 0000 | Process successfully completed. |
| EDE2001 | Authentication failed. Invalid API Username, Password or Token. |
| EDE2002 | Authentication failed. The API Token expired. |
| EDE2003 | Invalid API Request message format. Failed to retrieve Data Capsule. FILE_NAME = [[FILE_NAME]] |
| EDE2004 | Invalid API Request message format. Failed to retrieve Data Capsule Template. TEMPLATE_NAME = [[TEMPLATE_NAME]] |
| EDE2005 | Invalid API Request message format. |
| EDE2006 | Mandatory field required. |
| EDE2010 | DataSet [[TEMPLATE_NAME]] data does not match the Template format |
| EDE2011 | 1.	Check Logical Expression Fail in -><br />2.	Validation failure due to missing reference of Multivalue section record to unique key attribute of Single value section for Record No: "": Unique Key failed: "": Values: ""<br />3.	Maxlength of data more than defined for Record No: “”: Value: “”<br />4.	Invalid data type values for Record No: “”: Value: “”<br />5.	Data set does not match with the template set: Line No:<br />6.	Customer code Validation Failed<br />7.	Unique Fail-Null Value<br />8.	STATUS value failed<br />9.	Special character " present in Line No<br />10.	Special character '''' present in Line No<br />11.	Special character \| present in Line No<br />12.	Special character \ present in Line No<br />13.	Special character ^ present in Line No<br />14.	Special character ~ present in Line No<br />15.	Enum value failed<br />16.	Single value Lookup failed<br />17.	Multi value Lookup failed<br />18.	Uniqueness failed at file level<br />19.	Uniqueness failed<br />20.	Unique Key failed<br />21.	Unique value set not available for the multi value section<br />22.	Fill lookup failed.<br /> |
| EDE3000 | Import failed. Missing SYS_USER_CODE in Data Capsule. |
| EDE8000 | Some records failed to import. |
| EDE9000 | No record found. TEMPLATE_NAME = [[TEMPLATE_NAME]] |
| EDE9999 | Data Import process failed. |
```
